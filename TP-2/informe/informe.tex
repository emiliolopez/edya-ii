\documentclass[12pt]{article}
\usepackage{url,graphicx,tabularx,array,geometry}
\usepackage[latin1]{inputenc}
\usepackage[spanish]{babel}
\usepackage{minted}
\setlength{\parskip}{1ex} %--skip lines between paragraphs
\setlength{\parindent}{0pt} %--don't indent paragraphs

%-- Commands for header
\renewcommand{\title}[1]{\textbf{#1}\\}
\renewcommand{\line}{\begin{tabularx}{\textwidth}{X>{\raggedleft}X}\hline\\\end{tabularx}\\[-0.5cm]}
\newcommand{\leftright}[2]{\begin{tabularx}{\textwidth}{X>{\raggedleft}X}#1%
& #2\\\end{tabularx}\\[-0.5cm]}

%\linespread{2} %-- Uncomment for Double Space
\begin{document}

\title{Trabajo Práctico Nº 2}
\line
\leftright{\today}{López, Emilio \& Szturmaj, Ezequiel} %-- left and right positions in the header

\section{Especificaciones con ListSeq}

Analizaremos las especificaciones de costos para nuesta implementación de las funciones \emph{filterS}, \emph{showtS}, \emph{reduceS} y \emph{scanS}.

\subsection{Costos de \emph{filterS}}

\emph{filterS} para \emph{ListSeq} se encuentra implementada utilizando la función \emph{filter} para listas. Observando la implementación de Haskell, vemos

\begin{minted}{haskell}
filter :: (a -> Bool) -> [a] -> [a]
filter pred [] = []
filter pred (x:xs) | pred x    = x : filter pred xs
                   | otherwise = filter pred xs
\end{minted}

\begin{math}
W_{filter}(0) = 1 \\
W_{filter}(n) = 1 + W_{filter}(n-1) + W_{pred}(1)
\end{math}

Asumiendo que $W_{pred} \in O(1)$, tenemos

$$W_{filter}(n) - W_{filter}(n-1) = k$$

Luego, considerando $b_1 = 1$, $gr(p_1) = 0$, $p_1 = k$ la ecuación característica resulta

\begin{math}
(x-1) (x-1) = 0 \\
(x-1) ^ 2 = 0
\end{math}

Consecuentemente, tenemos dos raíces, y ambas valen $1$

\begin{math}
W_{filter}(n) = c_1 1^n + c_2 n 1^n \\
W_{filter}(0) = c_1 = 1 \\
W_{filter}(1) = c_1 + c_2 = 2 + k \Rightarrow c_2 = 1 + k  \\
W_{filter}(n) = 1 + (1^k) n = 1 + n + k n \Rightarrow W_{filter} \in O(n)
\end{math}

Si $W_{filter}$ no fuera constante, deberíamos considerar el costo de $W_{pred}$, que se ejecuta una vez por cada elemento, por lo que el costo de $W_{filter} \in O(n + \sum_{i=0}^n W_{pred}(i))$

Por lo tanto $W_{filterS} \in O(n + \sum_{i=0}^n W_{pred}(i))$. Similarmente, como \emph{filterS} no utiliza ninguna forma de paralelismo, $S_{filterS} \in O(n + \sum_{i=0}^n W_{pred}(i))$

\subsection{Costos de \emph{showtS}}

\emph{showtS} utiliza \emph{splitAt} y \emph{length}, por lo que deberemos analizar primero los órdenes de dichas funciones.

\begin{minted}{haskell}
splitAt :: Int -> [a] -> ([a], [a])
splitAt 0 xs     = ([], xs)
splitAt n []     = ([], [])
splitAt n (x:xs) = (x:xs', xs'')
    where
        (xs', xs'') = splitAt (n-1) xs''
\end{minted}

\begin{math}
W_{splitAt}(0) = 1 \\
W_{splitAt}(n) = 1 + W_{splitAt}(n-1)
\end{math}

Propongo que $W_{splitAt} \in O(n)$. Procederemos a probarlo

$$W_{splitAt}(n) - W_{splitAt}(n-1) = 1$$

Luego tomamos $b_1 = 1$, $p_{1}(n) = 1$ y $gr(p_{1}) = 0$ y procedemos a usar la ecuación característica

$$(x-1) (x-1) = (x-1)^2$$

Luego ambas raíces valen $1$

\begin{math}
W_{splitAt}(n) = c_1 1^n + c_2 n 1^n \\
W_{splitAt}(0) = c_1 = 1 \\
W_{splitAt}(1) = c_1 + c_2 = 2 \Rightarrow c_2 = 1
\end{math}

Luego $W_{splitAt}(n) = 1 + n \Rightarrow W_{splitAt} \in O(n)$

\emph{splitAt} no es paralelizable, por lo que $S_{splitAt} \in O(n)$ de la misma forma.

\begin{minted}{haskell}
length []     = 0
length (x:xs) = 1 + length xs
\end{minted}

\begin{math}
W_{length}(0) = 1 \\
W_{length}(n) = 1 + W_{length}(n-1)
\end{math}

Se puede observar a partir de $W_{length}$ que el orden de la función es el mismo que el de \emph{splitAt}. A su vez, como no es paralelizada en ninguna parte, su profundidad es la misma.

Ahora analizemos el costo de \emph{showtS}

\begin{math}
W_{showsT}(0) = 1 \\
W_{showsT}(1) = 1 \\
W_{showsT}(n) = 1 + W_{splitAt}(n) + W_{length}(n)
\end{math}

Ya que \emph{showsT} no es recursiva, y \emph{splitAt} y \emph{length} poseen $W \in O(n)$, podemos deducir que $W_{showsT} \in O(n)$. De forma similar, como \emph{showsT} no es paralelizada, $S_{showsT} \in O(n)$

\subsection{Costos de \emph{reduceS}}

\emph{reduceS} utiliza \emph{contractS}, por lo que deberemos analizar dicha función primero. A su vez, \emph{contractS} utiliza \emph{tabulateS}.

\begin{math}
W_{tabulateS}(0) = 1 \\
W_{tabulateS}(1) = W_{f}(0) \\
W_{tabulateS}(n) = 2 W_{tabulateS}(n/2) \\
\end{math}

\begin{math}
W_{tabulateS}(n) - 2 W_{tabulateS}(n/2) = 0 \\
T(i) = W_{tabulateS}(2^i) \\
T(i) = 2 T(i-1) \\
T(i) - 2 T(i-1) = 0
\end{math}

Aplicando la ecuación característica, obtenemos $(x-2)$. Luego la única raíz es $2$.
Procederemos a asumir que $W_{f} \in O(1)$

\begin{math}
T(i) = c 2^i \\
T(i) = W_{tabulateS}(2^i) \iff T(lg\ n) = W_{tabulateS}(n)
\textrm{ para n potencias de dos} \\
T(lg\ n) = c 2^{lg\ n} = c n = W_{tabulateS} \Rightarrow W_{tabulateS}(n) \in O(n)
\end{math}

En caso de que $W_{f} \notin O(1)$, deberemos agregar $\sum_{i=0}^n W_{f}(i)$ para tener en cuenta las $n$ ejecuciones de $f$ que se realizan durante \emph{tabulateS}, quedando $W_{tabulateS} \in O(n + \sum_{i=0}^n W_{f}(i))$

Ahora, veamos la profundidad

\begin{math}
S_{tabulateS}(0) = 1 \\
S_{tabulateS}(1) = 1 + S_{f}(1) \textrm{ asumiremos } S_{f} \in O(1) \\
S_{tabulateS}(n) = 1 + max (T(n/2), T(n/2)) + S_{++}(n/2) \\
S_{tabulateS}(n) = 1 + T(n/2) + S_{++}(n/2) \\
S_{tabulateS}(n) = 1 + T(n/2) + n
\end{math}

Haciendo cambio de variable, y asumiendo $S_{f}$ constante

\begin{math}
T(i) = S_{tabulateS}(2^i) \\
T(i) = T(i-1) + 2^i + k \\
T(i) - T(i-1) = 2^i + k
\end{math}

Considerando $b_1 = 1$, $p_1(m) = 1$, $gr(p_1) = 0$, $b_2 = 2$, $p_2(n) = 1$, $gr(p_2) = 0$, la ecuación característica resulta

$$(x-1)(x-1)(x-2) = 0$$
$$(x-1)^2(x-2) = 0$$

Esta ecuación tiene como raíz doble a $1$ y como raíz simple a $2$

\begin{math}
T(i) = c_1 1^i + c_2 i 1^i + c_3 2^i \\
T(i) = S_{tabulateS}(2^i) \iff S_{tabulateS}(n) = T(lg\ n) \\
T(lg\ n) = c_1 1^{lg\ n} + c_2\ lg\ n\ 1^{lg\ n} + c_3 n = S_{tabulateS}(n) \\
S_{tabulateS}(n) = c_1 + c2\ lg\ n + c_3 n \Rightarrow S_{tabulateS}(n) \in O(n) \\
S_{tabulateS}(1) = c_1 + c_3 = k \Rightarrow c_1 = k - c_3 \\
S_{tabulateS}(2) = c_1 + c_2 + 2 c_3 = k + 3 \\
S_{tabulateS}(2) = k - c_3 + c_2 + 2 c_3 = k + 3 \\
S_{tabulateS}(2) = c_2 + c_3 = 3 \Rightarrow c_2 = 3 - c_3 \\
S_{tabulateS}(4) = c_1 + 2 c_2 + 4 c_3 = k - c_3 + 6 - 2 c_3 + 4 c_3 = k + 8 \Rightarrow c_3 = 2, c_2 = 1, c_1 = k-2
\end{math}

Luego tenemos

$$S_{tabulateS}(n) = k-2 + lg\ n + 2 n \Rightarrow S_{tabulateS}(n) \in O(n)$$

Ahora, analizemos \emph{contractS}. Consideraremos \emph{f} como constante para este análisis

\begin{math} %\displaystyle
W_{contractS}(0) = W_{tabulateS}(0) = 1 \\
W_{contractS}(n) = W_{tabulateS}(n/2) \Rightarrow W_{contractS} \in O(n) \\
S_{contractS}(0) = S_{tabulateS}(0) = 1 \\
S_{contractS}(n) = S_{tabulateS}(n/2) \Rightarrow S_{contractS} \in O(n)
\end{math}

Si resultara que $f \notin O(n)$, deberemos agregar $\sum_{i=0}^n W_{f}(i)$ a $W_{contractS}$ y $max_{i=0}^n W_{f}(i)$. Ahora que conocemos los costos de \emph{contractS}, analizemos el de \emph{reduceS}

\begin{math}
W_{reduceS}(n) = 1 \\
W_{reduceS}(1) = W_{f}(c_1) \\
W_{reduceS}(n) = W_{reduceS}(n/2) + W_{contract}(n) = W_{reduceS}(n) + n
\end{math}

Haciendo cambio de variable

\begin{math}
T(i) = W_{reduceS}(2^i) \\
T(i) = T(i-1) + 2^i \\
T(i) - T(i-1) = 2^i
\end{math}

Luego, considerando $b_1 = 2$, $p_1(n) = 1$, y $gr(p_1) = 0$, la ecuación característica resulta

$$(x-1) (x-2) = 0$$

Luego las raíces valen $1$ y $2$.

\begin{math}
T(i) = c_1 1^i + c_2 2^i \\
T(0) = W_{reduceS}(1) = W_{f}(1)
\end{math}

Asumiendo $W_{f} \in O(1)$ resulta

\begin{math}
T(0) = k \\
T(0) = c_1 + c_2 = k \Rightarrow c_1 = k - c_2 \\
T(1) = W_{reduceS}(2) = W_{reduceS}(1) + W_{contractS}(2) = k + W_{tabulateS}(1) = k + k' \\
T(1) = c_1 + 2 c_2 = k - c_2 + 2 c_2 = k + k' \Rightarrow c_2 = k', c_1 = k-k' \\
T(i) = k - k' 1^i + k' 2^i \\
T(u) = W(2^u) \iff T(lg\ n) = W_{reduceS}(n) \forall n = 2^k, k \in N \\
W_{reduceS}(n) = k - k' + k' n \Rightarrow W_{reduceS}(n) \in O(n)
\end{math}

En caso de que $W_{f} \notin O(1)$, debemos considerar los costos de ejecutar $W_f$ de acuerdo al orden de reducción dado. De ello resulta $W_{reduceS} \in O(n + \sum_{(f, x, y) \in O_r} W_f(x, y))$, en donde $O_r$ es el conjunto de ejecuciones de f dadas en una cierta ejecución de \emph{reduceS}.

Si bien \emph{reduceS} es paralelizable, las funciones que poseen paralelismo poseen el mismo orden de trabajo y profundidad, por lo que el análisis de profundidad de \emph{reduceS} también resultará en $O(n + \sum_{(f, x, y) \in O_r} W_f(x, y))$


\subsection{Costos de \emph{scanS}}

\begin{math}
W_{scanS}(0) = 1 \\
W_{scanS}(1) = W_{singletonS}(1) + W_{f}(1) + W_{nthS}(1)
\end{math}

Asumiendo $W_f \in O(1)$

\begin{math}
W_{scanS}(1) = k \\
W_{scanS}(n) = W_{tabulate}(n) + W_{tabulate}(n/2) + W_{scanS}(n/2) \\
W_{scanS}(n) = W_{scanS}(n/2) + 2n
\end{math}

Ahora haciendo cambio de variable

\begin{math}
T(i) = W_{scanS}(2^i) \\
T(i) = T(i-1) + 2^{i+1} \\
T(i) - T(i-1) = 2^{i+1}
\end{math}

Ahora, considerando $b_1 = 2$, $p_1(i) = i$, $gr(p_1) = 0$ la ecuación característica resulta $(x-1) (x-2) = 0$ por lo que las raíces valen $1$ y $2$

\begin{math}
T(i) = c_1 1^i + c_2 2^i \\
T(0) = W_{scanS}(1) = k \Rightarrow c_1 + c_2 = k \Rightarrow c_1 = k - c_2 \\
T(2) = W_{scanS}(4) = W_{scanS}(2) + 8 = W_{scanS}(1) + 4 + 8 = k + 12 \Rightarrow c_1 + 4 c_2 = k + 3 c_2 = k + 12 \Rightarrow c_2 = 4, c_1 = k-4 \\
T(i) = k - 4 + 4 2^i \\
T(n) = W_{scanS}(2^n) \iff W_{scanS}(n) = T(lg\ n) \forall n = 2^k \\
W_{scanS}(n) = (k-4) + 4n \Rightarrow W_{scanS}(n) \in O(n) \\
\end{math}

Si $W_f \notin O(1)$, deberemos considerar el costo de todas las ejecuciones de f dado el orden de reducción, por lo que resulta $W_{scanS} \in O(n + \sum_{(f, x, y) \in O_r} W_f(x, y))$

En cuanto al análisis de profundidad, si bien \emph{scanS} posee cierto paralelismo al utilizar \emph{tabulateS}, su análisis de costo no se ve afectado ya que los órdenes de \emph{tabulateS} en trabajo y profundidad son el mismo. Por lo tanto, $S_{scanS} \in O(n + \sum_{(f, x, y) \in O_r} W_f(x, y))$

\section{Especificaciones con ArrSeq}

\subsection{Costos de \emph{filterS}}

Para analizar el costo de \emph{filterS}, deberemos mirar primero el de \emph{mapS} y el de \emph{joinS}. \emph{mapS} está implementado como un \emph{tabulate} con la función filtro, por lo que su trabajo termina siendo $O(\sum_{i=0}^n W_{f}(i))$, y su profundidad es $O(max_{i=0}^n W_{f}(i))$. \emph{joinS} a su vez está implementado con \emph{flatten}, por lo que su trabajo termina siendo $O(n)$ al ser una secuencia de secuencias de un elemento, y su profundidad, $O(lg\ n)$.

\begin{math}
W_{filterS}(n) = W_{joinS}(n) + W_{mapS}(n) + c \Rightarrow W_{filterS} \in O(n) + O(\sum_{i=0}^n W_{f}(i)) \\
S_{filterS}(n) = S_{joinS}(n) + S_{mapS}(n) + c \Rightarrow S_{filterS} \in O(lg\ n) + O(O(max_{i=0}^n W_{f}(i))
\end{math}

Luego, podemos concluir que $W_{filterS} \in O(\sum_{i=0}^n W_{f}(i))$ ya que la sumatoria siempre será al menos n, superando o igualando a O(n). En cuanto a la profundidad, podemos concluir que $S_{filterS} \in O(lg\ n\ max_{i=0}^n W_{f}(i))$

\subsection{Costos de \emph{showtS}}

Los costos de trabajo y profundidad de \emph{subArray}, \emph{length} y \emph{!} están dados por la implementación de \emph{Arr}, y son $O(1)$. \emph{takeS} y \emph{dropS} están implementados como llamadas a \emph{subArray} por lo que también poseen costo $O(1)$

\begin{math}
W_{showtS}(0) = 1 \\
W_{showtS}(1) = 1 + W_{!}(1) = k \in O(1) \\
W_{showtS}(n) = 1 + W_{takeS}(n) + W_{dropS}(n) + W_{length}(n) \\
W_{showtS}(n) = 1 + 2 W_{subArray}(n) + 2_{length}(n) = k \in O(1)
\end{math}

Luego, $W_{showtS} \in O(1)$. A su vez, como no poseen ninguna forma de paralelismo, $S_{showtS} \in O(1)$

\subsection{Costos de \emph{reduceS}}

\begin{math}
W_{reduceS}(0) = 1 \\
W_{reduceS}(1) = 1 + W_{nthS}(0) + W_{f}(1) = W_f + k \\
W_{reduceS}(n) = W_{reduceS}(n/2) + W_{contractS}(n) = W_reduceS(n/2) + n
\end{math}

Realizando cambio de variable, y asumiendo $W_{f} \in O(1)$ podemos observar que

\begin{math}
T(i) = W_{reduceS}(2^i) \\
T(i) = T(i-1) + 2^i \Rightarrow T(i) - T(i-1) = 2^i \\
\end{math}

Luego, considerando $b_1 = 2$, $p_1(i) = 1$ y $gr(p_1) = 0$, la ecuación característica resulta

$$(x-1) (x-2) = 0$$

Por lo que las raíces valen 1 y 2. Consecuentemente

\begin{math}
T(i) = c_1 1^i + c_2 2^i \\
T(0) = c_1 + c_2 = W_{reduceS}(1) = k \Rightarrow c_1 = k - c_2 \\
T(1) = c_1 + 2 c_2 = k - c_2 + 2 c_2 = k + c_2 = k + 1 \Rightarrow c_2 = 1 \\
T(1) = k - 1 +2^i \\
T(n) = W_{reduceS}(lg\ n) = k-1+n = 1 => W_{reduceS} \in O(n)
\end{math}

En cuanto a la profundidad

\begin{math}
S_{reduceS}(0) = 1 \\
S_{reduceS}(1) = 1, \textrm{ asumiendo } S_f(0) \in O(1) \\
S_{reduceS}(n) = S_{reduceS}(n/2) + S_{contractS}(n) \\
S_{reduceS}(n) = S_{reduceS}(n/2) + S_{tabulates}(n) \\
S_{reduceS}(n) = S_{reduceS}(n/2) + 1
\end{math}

Luego, la ecuación no homogénea resulta:

\begin{math}
S_{reduceS}(n) - S_{reduceS}(n/2) = 1
\end{math}

Realizo un cambio de variable, reemplazo $n = 2^n$. \\
Sea $T(i) = S_{reduceS}(2^i)$

\begin{math}
T(i) = T(i-1) + 1
T(i) - T(i - 1) = 1
\end{math}

La ecuación característica de las recurrencias resulta $(x - 1)^2 = 0$, siendo $b_1 = 1$, $p_1(n) = 1$, $gr(p1) = 0$

Las raíces de la ecuación son $r_1 = r_2 = 1$

Con lo cual, $T(i) = c_1 1^i + c_2 1^i$

\begin{math}
T(0) = S_{reduceS}(1) = 1 \Rightarrow c_1 = 1 \\
T(1) = S_{reduceS}(2) = 2 \Rightarrow c_1 + c_2 = 1 + c_2 = 2 \Rightarrow c_2 = 1
\end{math}

Sustituyendo las constantes $c_1$ y $c_2$

\begin{math}
T(i) = 1 1^i + i 1^i = i \\
T(n) = S_{reduceS}(n) \iff S_{reduceS}(n) = T(lg\ n) \forall n = 2^k \\
S_{reduceS}(n) = lg\ n \Rightarrow S_{reduceS}(n) \in O(lg n)
\end{math}

Si $S_f \notin O(1) \Rightarrow S_{reduceS}(n) \in O(lg\ n\ \sum_{(f, x, y) \in O_r} S_f(x, y))$

\subsection{Costos de \emph{scanS}}

\begin{math}
W_{scanS}(0) = 1 \\
W_{scanS}(1) = W_{singletonS}(1) + W_{f}(1) + W_{nthS}(1) = k \in O(1) \\
W_{scanS}(n) = W_{tabulateS}(n) + W_{tabulateS}(n/2) + W_{scanS} (n/2)
\end{math}

Considerando $W_f \in O(1)$ resulta

\begin{math}
W_{tabulateS}(n) \in O(n) \\
W_{tabulateS}(n/2) \in O(n)
\end{math}

Luego, $W_{scanS}(n) = W_{scanS}(n/2) + 2 n$. Haciendo cambio de variable,

\begin{math}
T(i) = W_{scanS}(2^i) \\
T(i) = T(i-1) + 2^{i+1} \\
T(i) - T(i-1) = 2^{i+1}
\end{math}

De esto resulta $b_1 = 2$, $p_1(i) = 1$ y $gr(p_1) = 0$, por lo que la ecuación característica es $(x-1) (x-2) = 0$ y las raíces son $1$ y $2$

\begin{math}
T(i) = c_1 1^i + c_2 2^i \\
T(0) = W_{scanS}(1) = k \Rightarrow c_1 + c_2 = k \Rightarrow c_1 = k - c_2 \\
T(1) = W_{scanS}(2) = k + 4 \Rightarrow c_1 + 2 c_2 = k - c_2 + 2 c_2 = k + c_2 = k + 4 \Rightarrow c_2 = 4, c_1 = k - 4
\end{math}

Luego

\begin{math}
T(i) = (k-4) + 2^{i+2} \\
T(i) = W_{scanS}(2^n) \iff T(lg\ n) = W_{scanS}(n) \\
W_{scanS}(n) = k - 4 + 4 n \Rightarrow W_{scanS} \in O(n)
\end{math}

Si $W_f \notin O(1)$, deberemos considerar el trabajo de todas las ejecuciones de f para el orden de reducción dado, quedando el costo de $W_{scanS} \in O(n + \sum_{(f, x, y) \in O_r} W_f(x, y))$

Ahora analizemos la profundidad

\begin{math}
S_{scanS}(0) = 1 \\
S_{scanS}(1) = 1 \textrm{ asumiendo } S_{f} \in O(1) \\
S_{scanS}(n) = S_{tabulateS}(n) + S_{tabulateS}(n/2) + S_{scanS}(n/2) \\
S_{scanS}(n) = 2 + S_{scanS}(n/2)
\end{math}

Haciendo cambio de variable, vemos que

\begin{math}
T(i) = T(i-1) + 2 \\
T(i) - T(i-1) = 2
\end{math}

Por lo que tomando $b_1 = 1$, $p_1(i) = 2$ y $gr(p_1) = 0$, la ecuación característica resulta $(x-1)(x-1) = 0$ y se desprende que el polinomio posee dos raíces, ambas valiendo $1$.

\begin{math}
T(i) = c_1 1^i + c_2 1^i i \\
T(0) = c_1 = 1 \\
T(1) = c_1 + c_2 = 1 + c_2 = 3 \Rightarrow c_2 = 2 \\
T(i) = 1^i + 2 1^i i = 1 + 2i \\
T(n) = S_{scanS}(2^n) \iff T(lg\ n) = S_{scanS}(n) \forall n = 2^k \\
S_{scanS}(n) = 1 + lg\ n \Rightarrow S_{scanS}(n) \in O(lg\ n)
\end{math}

En el caso de que $S_{f} \notin O(1)$, deberemos considerar el trabajo de todos las ejecuciones de f para el orden de reducción dado, realizadas en simultáneo, quedando el costo de $S_{scanS} \in O(lg\ n \sum_{(f, x, y) \in O_r} S_f(x, y))$
\end{document}
