module ListSeq where

import Par
import Seq

instance Seq [] where
   -- empty sequence
   emptyS               = []
   -- sequence with just one element
   singletonS a         = [a]
   -- length of sequence
   lengthS a            = length a
   -- get nth element of the sequence
   nthS a n             = a !! n
   -- get a sequence like <f 0, f 1, ..., f n-1>
   tabulateS f 0        = []
   tabulateS f 1        = [f 0]
   tabulateS f n        = tabL ++ tabR
      where
         sizeL          = n `div` 2
         sizeR          = n - sizeL
         tabL'          = tabulateS f sizeL
         tabR'          = tabulateS (f . (+sizeL)) sizeR
         (tabL, tabR)   = tabL' ||| tabR'
   -- map a function to a sequence
   mapS f a             = map f a
   -- filter a sequence, leaving elements where f e == True
   filterS f a          = filter f a
   -- concatenate two sequences
   appendS a b          = a ++ b
   -- return the first n elements of a sequence
   takeS a n            = take n a
   -- skip the first n elements of a sequence and return the rest
   dropS a n            = drop n a
   -- convert a sequence into a TreeView
   showtS []            = EMPTY
   showtS [x]           = ELT x
   showtS xs            = NODE (ys) (zs)
      where
         (ys, zs)       = splitAt n xs
         n              = (length xs) `div` 2
   -- convert a sequence into a ListView
   showlS []            = NIL
   showlS (x:xs)        = CONS x xs
   -- flatten a sequence of sequences into a sequence
   joinS a              = concat a
   -- make a sequence from a list
   fromList l           = l

type ListSeq t = [t]
