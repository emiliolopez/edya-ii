module ArrSeq where

import Seq
import qualified Arr as A
import Arr ((!))

instance Seq A.Arr where
   -- empty sequence
   emptyS          = A.empty
   -- sequence with just one element
   singletonS a    = A.fromList [a]
   -- length of sequence
   lengthS a       = A.length a
   -- get nth element of the sequence
   nthS a n        = a ! n
   -- get a sequence like <f 0, f 1, ..., f n-1>
   tabulateS f n   = A.tabulate f n
   -- map a function to a sequence
   mapS f a        = A.tabulate (\x -> f $ nthS a x) $ lengthS a
   -- filter a sequence, leaving elements where f e == True
   filterS f a     = joinS $ mapS (\x -> if f x then singletonS x else emptyS) a
   -- concatenate two sequences
   appendS a b     = tabulateS getElem tlen
      where
         alen = lengthS a
         blen = lengthS b
         tlen = alen + blen
         getElem = (\x -> if x < alen then nthS a x else nthS b (x-alen))
   -- return the first n elements of a sequence
   takeS a n       = A.subArray 0 n a
   -- skip the first n elements of a sequence and return the rest
   dropS a n       = A.subArray n ((A.length a) - n) a
   -- convert a sequence into a TreeView
   showtS xs | A.length xs == 0  = EMPTY
             | A.length xs == 1  = ELT (xs ! 0)
             | otherwise         = NODE ys zs
      where
         ys = takeS xs n
         zs = dropS xs n
         n  = (lengthS xs) `div` 2
   -- convert a sequence into a ListView
   showlS xs | A.length xs == 0 = NIL
             | otherwise        = CONS x ys
      where
         x  = xs ! 0
         ys = dropS xs 1
   -- flatten a sequence of sequences into a sequence
   joinS a         = A.flatten a
   -- make a sequence from a list
   fromList l      = A.fromList l

type ArrSeq t = A.Arr t
