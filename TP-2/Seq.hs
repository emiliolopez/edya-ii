{- Implementación del TAD secuencia -}

module Seq where

class Seq s where
   emptyS     :: s a
   singletonS :: a -> s a
   lengthS    :: s a -> Int 
   nthS       :: s a -> Int -> a 
   tabulateS  :: (Int -> a) -> Int -> s a
   mapS       :: (a -> b) -> s a -> s b 
   filterS    :: (a -> Bool) -> s a -> s a 
   appendS    :: s a -> s a -> s a
   takeS      :: s a -> Int -> s a
   dropS      :: s a -> Int -> s a
   showtS     :: s a -> TreeView a (s a)
   showlS     :: s a -> ListView a (s a)
   joinS      :: s (s a) -> s a
   -- reduce a sequence into an element
   reduceS    :: (a -> a -> a) -> a -> s a -> a
   reduceS f b s | slength == 0 = b
                 | slength == 1 = f b $ nthS s 0
                 | otherwise    = reduceS f b $ contractS f s
      where
         slength = lengthS s
   -- reduce a sequence into an element, and show all intermediate steps
   scanS      :: (a -> a -> a) -> a -> s a -> (s a, a)
   scanS f b s | slength == 0 = (emptyS, b)   -- make a sequence from a list
               | slength == 1 = (singletonS b, f b (nthS s 0))
               | otherwise    = (expand scanR, reduceR)
      where
         slength          = lengthS s
         (scanR, reduceR) = scanS f b $ contractS f s
         expandR          = (\x -> if even x then nthS scanR (x `div` 2) else f (nthS scanR (x `div` 2)) (nthS s (x-1)))
         expand c         = tabulateS expandR slength
   fromList   :: [a] -> s a

data TreeView a t = EMPTY | ELT a | NODE t t deriving (Show)
data ListView a t = NIL | CONS a t deriving (Show)

-- helper function: contract a sequence
contractS :: Seq s => (a -> a -> a) -> s a -> s a
contractS f s | contractLength * 2 == slength = tabulateS contractGenEven contractLength
              | otherwise                     = tabulateS contractGenOdd contractLength
   where
      slength          = lengthS s
      contractLength   = (slength + 2 - 1) `div` 2 -- ceiling
      contractGenEven  = (\x -> f (nthS s (2*x)) (nthS s (2*x+1)))
      contractGenOdd   = (\x -> if x == contractLength-1 then (nthS s (2*x)) else contractGenEven x)
