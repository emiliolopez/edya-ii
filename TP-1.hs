{-# LANGUAGE TemplateHaskell #-}

module TP1 where

import Test.QuickCheck
import Text.Show.Pretty

class Diccionario t where
    vacia    :: Ord k => t k v
    insertar :: Ord k => (k, v) -> t k v -> t k v
    eliminar :: Ord k => k -> t k v -> t k v
    buscar   :: Ord k => k -> t k v -> Maybe v

instance Diccionario BTree32 where
    vacia    = Nil
    insertar = insert
    eliminar = delete
    buscar   = lookup'

data BTree32 k a = Nil             -- empty tree
                 | Node
                     (BTree32 k a) -- left subtree
                     Int           -- tree size
                     (k, a)        -- node element
                     (BTree32 k a) -- right subtree
        deriving (Show, Eq)

-- Returns tree size. Constant cost
size :: BTree32 k a -> Int
size Nil = 0
size (Node _ s _ _) = s

-- Returns the value associated with a key
lookup' :: Ord k => k -> BTree32 k a -> Maybe a
lookup' _ Nil = Nothing
lookup' s (Node l _ (k, a) r) | s == k = Just a
                              | s > k  = lookup' s r
                              | s < k  = lookup' s l

-- single tree rotation to the left, see figure 1 on pdf
singleL :: BTree32 k a -> BTree32 k a
singleL (Node a tam vala (Node lb _ valb d)) = (Node treeabc tam valb d)
        where
                sl = size a + size lb + 1
                treeabc = (Node a sl vala lb)
singleL x = x

-- single tree rotation to the right, see figure 1 on pdf
singleR :: BTree32 k a -> BTree32 k a
singleR (Node (Node ll _ l lr) s x r) = (Node ll s l (Node lr t x r))
        where
                t = size lr + size r + 1
singleR x = x

-- double tree rotation to the left, see figure 1 on pdf
doubleL :: BTree32 k a -> BTree32 k a
doubleL (Node l s v r) = singleL (Node l s v (singleR r))

-- double tree rotation to the right, see figure 1 on pdf
doubleR :: BTree32 k a -> BTree32 k a
doubleR (Node l s v r) = singleR (Node (singleL l) s v r)

-- Given two trees and a key-value set, returns a new tree, while keeping
-- the balance BST property
balance :: BTree32 k a -> (k , a) -> BTree32 k a -> BTree32 k a
balance l x r | size l + size r <= 1 = t
              | violates1 && needsSingleL = singleL t
              | violates1 && needsDoubleL = doubleL t
              | violates2 && needsSingleR = singleR t
              | violates2 && needsDoubleR = doubleR t
              | otherwise = t
        where
                (Node rl _ _ rr) = r
                (Node ll _ _ lr) = l
                t = (Node l (size r + size l + 1) x r)
                violates1 = size r >  3 * size l
                needsSingleL = size rl < 2 * size rr
                needsDoubleL = not needsSingleL
                violates2 = size l > 3 * size r
                needsSingleR = size ll < 2 * size lr
                needsDoubleR = not needsSingleR

-- Adds a key-value set to the tree
insert :: Ord k => (k, a) -> BTree32 k a -> BTree32 k a
insert val Nil = (Node Nil 1 val Nil)
insert val@(k, _) (Node l s v@(x, _) r) | k > x     = balance l v nr
                                        | k == x    = (Node l s val r)
                                        | otherwise = balance nl v r
        where
                nr = insert val r
                nl = insert val l

maximum' :: BTree32 k a -> (k, a)
maximum' (Node _ _ x Nil) = x
maximum' (Node _ _ _ r) = maximum' r

-- Deletes the tree root
delRoot :: Ord k => BTree32 k a -> BTree32 k a
delRoot Nil = Nil
delRoot node@(Node _ _ (k, _) _) = delete k node

-- Delete a key-value set from the tree
delete :: Ord k => k -> BTree32 k a -> BTree32 k a
delete _ Nil = Nil
delete s node@(Node Nil _ (k, _) Nil) | s == k = Nil
                                      | otherwise = node
delete s (Node Nil _ (k, _) r) | s == k = r
delete s (Node l _ (k, v) r) | s == k = balance (delete midkey l) mid r
                             | s > k  = (Node l sizer (k, v) cleanr)
                             | s < k  = (Node cleanl sizel (k, v) r)
        where
                cleanr = delete s r
                cleanl = delete s l
                sizer = size cleanr + size l + 1
                sizel = size r + size cleanl + 1
                mid@(midkey, _) = maximum' l


-- Example trees

type IntDic = BTree32 Int Int

printTree x = putStrLn $ ppShow x

tree1 :: IntDic
tree1 = let tree = vacia :: IntDic
            tree2 = insertar (10,10) tree
            tree3 = insertar (20,20) tree2
            tree4 = insertar (30,30) tree3
            tree5 = insertar (40,40) tree4
            tree6 = insertar (50,50) tree5
            tree7 = insertar (60,60) tree6
            tree8 = insertar (70,70) tree7
            tree9 = insertar (80,80) tree8
        in tree9

tree2 :: IntDic
tree2 = (Node l 3 a (Node ll 3 b d))
        where
               l = (Node Nil 1 (2,2) Nil)
               a = (1,1)
               ll = (Node (Node Nil 1 (6,6) Nil) 2 (4,4) (Node Nil 1 (7,7) Nil))
               b = (3,3)
               d = (Node Nil 1 (5,5) Nil)


-- QuickCheck tests

runTests = $quickCheckAll

-- Empty trees have zero size
prop_sizeNil = size Nil == 0

-- Single rotations are reversible
prop_singleRot1 = singleR (singleL tree1) == tree1
prop_singleRot2 = singleL (singleR tree2) == tree2

-- Trees always grow when we add an element to them
prop_treeGrows n = let k = abs(n) + 81 in size (insertar (k, k) tree1) == size tree1 + 1
        where
                types = n :: Int

-- Trees always shrink when we remove one element from them
prop_treeShrinks n = let k = ((abs(n) `mod` 8) + 1) * 10 in size (eliminar k tree1) == size tree1 - 1
        where
                types = n :: Int

-- Insert makes it possible to lookup values
prop_insertEnablesLookup n = buscar n (insertar (n, n) tree1) == Just n
        where
                types = n :: Int

-- Insert always updates existing value
prop_insertUpdatesValue n = (buscar n $ insertar (n, 42) $ insertar (n, n) tree1) == Just 42
        where
                types = n :: Int

-- Delete always makes values disappear
prop_deleteRemovesValues n = (buscar n $ eliminar n $ insertar (n, n) tree1) == Nothing
        where
                types = n :: Int

isBalanced :: BTree32 k a -> (k, a) -> BTree32 k a -> Bool
isBalanced l _ r | not (violates1 || violates2) = True
                 | otherwise                    = False
        where
            violates1 = size r >  3 * size l
            violates2 = size l > 3 * size r

-- Inserting elements keeps the tree balanced
prop_insertingKeepsBalance n = isBalanced l x r == True
        where
                (Node l _ x r) = insertar (n, n) tree2
                types = n :: Int

-- Deleting elements keeps the tree balanced
prop_deleteKeepsBalanced n = isBalanced l x r == True
        where
                k = ((abs(n) `mod` 8) + 1) * 10
                (Node l _ x r) = eliminar k tree1
                types = n :: Int
