module Lab01 where

import Data.List
import Data.Char

{-
1) Corregir los siguientes programas de modo que sean aceptados por GHCi.
-}

-- a)
not' :: Bool -> Bool
not' b = case b of
    True -> False
    False -> True

-- b)
in' :: [a] -> [a]
in' [x]    = []
in' (x:xs) = x : in' xs
in' []     = error "empty list"

-- c)
length' :: Num b => [a] -> b
length' []    = 0
length' (_:l) = 1 + length' l

-- d)
list123 :: Num a => [a]
list123 = 1 : 2 : 3 : []

-- e)
(++!) :: [a] -> [a] -> [a]
[]     ++! ys = ys
(x:xs) ++! ys = x : xs ++! ys

-- f)
addToTail :: Num a => a -> [a] -> [a]
addToTail x (xh:xs) = xh : map (+x) xs

-- g)
listmin :: Ord a => [a] -> a
listmin xs = head (sort xs)

-- h) (*)
smap :: (a -> a) -> [a] -> [a]
smap f [] = []
smap f [x] = [f x]
smap f (x:xs) = f x : smap f xs

{-
2. Definir las siguientes funciones y determinar su tipo:

a) five, que dado cualquier valor, devuelve 5

b) apply, que toma una función y un valor, y devuelve el resultado de
aplicar la función al valor dado

c) ident, la función identidad

d) first, que toma un par ordenado, y devuelve su primera componente

e) derive, que aproxima la derivada de una función dada en un punto dado

f) sign, la función signo

g) vabs, la función valor absoluto (usando sign y sin usarla)

h) pot, que toma un entero y un número, y devuelve el resultado de
elevar el segundo a la potencia dada por el primero

i) xor, el operador de disyunción exclusiva

j) max3, que toma tres números enteros y devuelve el máximo entre llos

k) swap, que toma un par y devuelve el par con sus componentes invertidas
-}

five :: Num b => a -> b
five _ = 5

apply :: (a -> b) -> a -> b
apply f x = f x

ident :: a -> a
ident x = x

first :: (a, b) -> a
first (a, _) = a

derive :: (Fractional a) => (a -> a) -> a -> a
derive f x = ((f (x+h)) - (f x)) / h
    where h = 0.001

sign :: (Real a) => a -> a
sign x | x < 0     = -1
       | x == 0    = 0
       | otherwise = 1

vabs :: (Real a) => a -> a
vabs x = (sign x) * x

vabs' :: (Real a) => a -> a
vabs' x | x < 0     = -x
        | otherwise = x

pot :: Num a => Integer -> a -> a
pot 1 y = y
pot 0 y = 1
pot x y = y * (pot (x-1) y)

xor :: Bool -> Bool -> Bool
xor x y | x == y = False
        | otherwise = True

max3 :: Ord a => a -> a -> a -> a
max3 x y z | x > y = max x z
           | otherwise = max y z

swap :: (a, b) -> (b, a)
swap (a, b) = (b, a)

{- 
3) Definir una función que determine si un año es bisiesto o no, de
acuerdo a la siguiente definición:

año bisiesto 1. m. El que tiene un día más que el año común, añadido al mes de febrero. Se repite
cada cuatro años, a excepción del último de cada siglo cuyo número de centenas no sea múltiplo
de cuatro. (Diccionario de la Real Academia Espaola, 22ª ed.)

¿Cuál es el tipo de la función definida?
-}

bisiesto :: Integral a => a -> Bool
bisiesto x = (x `mod` 400 == 0) || ((x `mod` 4 == 0) && (x `mod` 100 /= 0))

{-
4)

Defina un operador infijo *$ que implemente la multiplicación de un
vector por un escalar. Representaremos a los vectores mediante listas
de Haskell. Así, dada una lista ns y un número n, el valor ns *$ n
debe ser igual a la lista ns con todos sus elementos multiplicados por
n. Por ejemplo,

[ 2, 3 ] *$ 5 == [ 10 , 15 ].

El operador *$ debe definirse de manera que la siguiente
expresión sea válida:

-}

(*$) :: Num a => [a] -> a -> [a]
(x:xs) *$ n = x * n : xs *$ n
[] *$ _ = []

-- v = [1, 2, 3] *$ 2 *$ 4


{-
5) Definir las siguientes funciones usando listas por comprensión:

a) 'divisors', que dado un entero positivo 'x' devuelve la
lista de los divisores de 'x' (o la lista vacía si el entero no es positivo)

b) 'matches', que dados un entero 'x' y una lista de enteros descarta
de la lista los elementos distintos a 'x'

c) 'cuadrupla', que dado un entero 'n', devuelve todas las cuadruplas
'(a,b,c,d)' que satisfacen a^2 + b^2 = c^2 + d^2,
donde 0 <= a, b, c, d <= 'n'

(d) 'unique', que dada una lista 'xs' de enteros, devuelve la lista
'xs' sin elementos repetidos
-}

divisors :: Integral a => a -> [a]
divisors x = [y | y <- [1..x], x `mod` y == 0]

matches :: (Num a, Eq a) => a -> [a] -> [a]
matches _ [] = []
matches x (y:ys) | x == y = x : matches x ys
                 | otherwise = matches x ys

cuadrupla :: (Num a, Eq a, Enum a) => a -> [(a,a,a,a)]
cuadrupla a = [(b,c,d,e) | b <- [0..a], c <- [0..a], d <- [0..a], e <- [0..a], b^2 + c^2 == d^2 + e^2]

unique :: (Num a, Eq a) => [a] -> [a]
unique [] = []
unique (a:as) | a `elem` as = unique as
              | otherwise = a : unique as


{- 
6) El producto escalar de dos listas de enteros de igual longitud
es la suma de los productos de los elementos sucesivos (misma
posición) de ambas listas.  Definir una función 'scalarProduct' que
devuelva el producto escalar de dos listas.

Sugerencia: Usar las funciones 'zip' y 'sum'. -}

scalarProduct :: Num a => [a] -> [a] -> a
scalarProduct a b = sum (zipWith (*) a b)

{-
7) Definir mediante recursión explícita
las siguientes funciones y escribir su tipo más general:

a) 'suma', que suma todos los elementos de una lista de números
-}

suma :: Num a => [a] -> a
suma [] = 0
suma (x:xs) = x + suma xs

{-
b) 'alguno', que devuelve True si algún elemento de una
lista de valores booleanos es True, y False en caso
contrario
-}

alguno :: [Bool] -> Bool
alguno [] = False
alguno (True:xs) = True
alguno (False:xs) = alguno xs

{-
c) 'todos', que devuelve True si todos los elementos de
una lista de valores booleanos son True, y False en caso
contrario
-}

todos :: [Bool] -> Bool
todos [] = True
todos (True:xs) = todos xs
todos (False:xs) = False

{-
d) 'codes', que dada una lista de caracteres, devuelve la
lista de sus ordinales
-}

codes :: String -> [Int]
codes [] = []
codes (x:xs) = ord x : codes xs

{-
e) 'restos', que calcula la lista de los restos de la
división de los elementos de una lista de números dada por otro
número dado
-}

restos :: [Integer] -> Integer -> [Integer]
restos [] _ = []
restos (a:as) x = a `mod` x : restos as x

{-
f) 'cuadrados', que dada una lista de números, devuelva la
lista de sus cuadrados
-}

cuadrados :: Num a => [a] -> [a]
cuadrados [] = []
cuadrados (x:xs) = x ^ 2 : cuadrados xs

{-
g) 'longitudes', que dada una lista de listas, devuelve la
lista de sus longitudes
-}

longitudes :: [[a]] -> [Int]
longitudes [] = []
longitudes (x:xs) = length x : longitudes xs

{-
h) 'orden', que dada una lista de pares de números, devuelve
la lista de aquellos pares en los que la primera componente es
menor que el triple de la segunda
-}

orden :: (Num a, Ord a) => [(a,a)] -> [(a,a)]
orden [] = []
orden ((x,y):xs) | x < (3*y) = (x,y) : orden xs
                 | otherwise = orden xs

{-
i) 'pares', que dada una lista de enteros, devuelve la lista
de los elementos pares
-}

pares :: Integral a => [a] -> [a]
pares [] = []
pares (x:xs) | x `mod` 2 == 0 = x : pares xs
             | otherwise      = pares xs

{-
j) 'letras', que dada una lista de caracteres, devuelve la
lista de aquellos que son letras (minúsculas o mayúsculas)
-}

letras :: String -> String
letras [] = []
letras (x:xs) | x >= 'A' && x <= 'Z' = x : letras xs
              | x >= 'a' && x <= 'z' = x : letras xs
              | otherwise = letras xs

{-
k) 'masDe', que dada una lista de listas 'xss' y un
número 'n', devuelve la lista de aquellas listas de 'xss'
con longitud mayor que 'n'
-}

masDe :: [[a]] -> Int -> [[a]]
masDe [] _ = []
masDe (x:xs) n | length x > n = x : masDe xs n
               | otherwise    = masDe xs n
